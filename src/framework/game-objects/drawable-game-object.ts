import { Vector2D } from "../vector2d";
import { GameObject } from "./game-object";

export abstract class DrawableGameObject extends GameObject {
    protected position: Vector2D = Vector2D.Zero;

    public abstract draw(ctx: CanvasRenderingContext2D): void;
}