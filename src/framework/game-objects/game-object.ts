export abstract class GameObject {
    public abstract update(ctx: CanvasRenderingContext2D): void;
}