export class Vector2D {
    constructor(
        public x: number = 0,
        public y: number = 0
    ) {

    }

    public static Zero: Vector2D = new Vector2D(0, 0);
}