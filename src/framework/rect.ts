import { Vector2D } from "./index";

export class Rect {
    constructor(
        public topLeft: Vector2D,
        public bottomRight: Vector2D
    ) {

    }
}