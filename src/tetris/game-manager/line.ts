import { Config } from "../variables";
import { Node } from "./index";
import { Rect } from "../../framework/index";
import { DrawableGameObject } from "../../framework/game-objects/index";

export class Line extends DrawableGameObject {
    private nodes: Node[] = new Array(Config.NumberOfColumns);

    constructor() {
        super();
        
    }
    
    public update(ctx: CanvasRenderingContext2D): void {
        
    }

    public draw(ctx: CanvasRenderingContext2D): void {
        
    }
}