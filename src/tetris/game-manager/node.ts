import { Config } from "../variables";
import { Rect } from "../../framework/index";
import { DrawableGameObject } from "../../framework/game-objects/index";

export class Node extends DrawableGameObject {
    public active: boolean = false;
    public color: string = Config.BackgroundColor;
    public rect: Rect;

    constructor() {
        super();

    }

    public update(ctx: CanvasRenderingContext2D): void {
        throw new Error("Method not implemented.");
    }

    public draw(ctx: CanvasRenderingContext2D): void {
        if (this.active) {
            ctx.fillStyle = this.color;
            ctx.strokeStyle = Config.RectStrokeColor;

            ctx.strokeRect(
                this.rect.bottomRight.x,
                this.rect.topLeft.y,
                this.rect.topLeft.x - this.rect.bottomRight.x,
                this.rect.bottomRight.y - this.rect.topLeft.y
            );

            ctx.fillRect(
                this.rect.bottomRight.x,
                this.rect.topLeft.y,
                this.rect.topLeft.x - this.rect.bottomRight.x,
                this.rect.bottomRight.y - this.rect.topLeft.y
            );
        }
    }
}