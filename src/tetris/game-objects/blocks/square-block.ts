import { Block } from "./block";
import { Config, Colors } from "../../index";
import { Vector2D, Rect } from "../../../framework/index";

export class SquareBlock extends Block {
    constructor() {
        super();

        this.color = Colors.Red;

        this.rects[0] = new Rect(new Vector2D(0, 0),
                                 new Vector2D(Config.RectWidth, Config.RectHeight));

        this.rects[1] = new Rect(new Vector2D(Config.RectWidth, 0),
                                 new Vector2D(Config.RectWidth * 2, Config.RectHeight));

        this.rects[2] = new Rect(new Vector2D(0, Config.RectHeight),
                                 new Vector2D(Config.RectWidth, Config.RectHeight * 2));

        this.rects[3] = new Rect(new Vector2D(Config.RectWidth, Config.RectHeight),
                                 new Vector2D(Config.RectWidth * 2, Config.RectHeight * 2));
    }

    public update(ctx: CanvasRenderingContext2D) {
        super.update(ctx);
    }

    public draw(ctx: CanvasRenderingContext2D): void {
        super.draw(ctx);
    }
}