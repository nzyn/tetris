import { Config, Colors } from "../../index";
import { Rect } from "../../../framework/index";
import { DrawableGameObject } from "../../../framework/game-objects/index";

export abstract class Block extends DrawableGameObject {
    protected rects: Rect[];
    protected color: string = "#FFFFFF";

    constructor() {
        super();
        this.rects = [];
    }

    public update(ctx: CanvasRenderingContext2D) {

    }

    public draw(ctx: CanvasRenderingContext2D): void {
        ctx.fillStyle = Colors.Red;
        ctx.strokeStyle = Config.RectStrokeColor;

        for (var i = 0; i < this.rects.length; i++) {
            var rect = this.rects[i];

            ctx.strokeRect(
                rect.bottomRight.x,
                rect.topLeft.y,
                rect.topLeft.x - rect.bottomRight.x,
                rect.bottomRight.y - rect.topLeft.y
            );

            ctx.fillRect(
                rect.bottomRight.x,
                rect.topLeft.y,
                rect.topLeft.x - rect.bottomRight.x,
                rect.bottomRight.y - rect.topLeft.y
            );
        }
    }
}