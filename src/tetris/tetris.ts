import { bindable } from "aurelia-framework";
import { Config, Colors } from "./index";
import { Block, SquareBlock } from "./game-objects/blocks/index";

export class Tetris {
    private canvas: HTMLCanvasElement;
    private blocks: Block[] = [];

    private canvasWidth = Config.CanvasWidth;
    private canvasHeight = Config.CanvasHeight;

    constructor() {
        this.blocks.push(new SquareBlock());
        this.blocks.push(new SquareBlock());
    }

    attached() {
        let ctx = this.canvas.getContext("2d");

        let interval = setInterval(() => {
            ctx.lineWidth = 0;
            ctx.fillStyle = Config.BackgroundColor;
            ctx.fillRect(0, 0, Config.CanvasWidth, Config.CanvasHeight);
            ctx.lineWidth = Config.StrokeWidth;

            for (var i = 0; i < this.blocks.length; i++) {
                let block = this.blocks[i];

                block.update(ctx);
                block.draw(ctx);
            }

        }, Config.RefreshRate);
    }
}