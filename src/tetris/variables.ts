export class Colors {
    public static Black: string = "#000000";
    public static White: string = "#FFFFFF";
    public static Red: string = "#FF0000";
}

export class Config {
    public static NumberOfColumns: number = 10;

    public static CanvasHeight: number = Math.floor(window.innerHeight / 100) * 100;
    public static CanvasWidth: number = Config.CanvasHeight / 2;

    public static RectWidth: number = Config.CanvasWidth / Config.NumberOfColumns;
    public static RectHeight: number = Config.RectWidth;

    public static StrokeWidth: number = 2;

    public static RefreshRate: number = 1000 / 15;

    public static BackgroundColor: string = Colors.Black;
    public static RectStrokeColor: string = Config.BackgroundColor;
}